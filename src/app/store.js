import { configureStore } from '@reduxjs/toolkit'
import serwisReducer from 'features/serwis/serwisSlice'
import authReducer from 'features/serwis/authSlice'

export default configureStore({
  reducer: {
    dropWater: serwisReducer,
     auth: authReducer
},

})
