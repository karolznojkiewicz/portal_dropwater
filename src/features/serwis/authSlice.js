import {createSlice} from '@reduxjs/toolkit'
import { auth } from 'services/config'

const initialState = {
    login: false,
    user: null
}
export const slice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        loginIn: (state, action) => {
            state.login = true
            state.user = action.payload
        },
        logout: (state) => {
            auth().signOut()
            state.login = false
            state.user = null
        }
    }
})

export const {
    loginIn, logout
} = slice.actions

export const selectCurrentUser = state => state.auth.user
export const userLogin = state => state.auth.login


export default slice.reducer