import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  klienci: [
    {
      id: 1,
      name: 'Karol Znojkiewicz',
      street: 'os.Cegielskiego 39/5',
      city: 'Poznań',
      phone: ['623742381', '321317224'],
    },
    {
      id: 2,
      name: 'Patryk Marcinowski',
      street: 'os.Marcinkowskiego 5',
      city: 'Czerwonak',
      phone: ['623742383'],
    },
  ],
  serwisy: [
    {
      id: 1,
      name: 'Karol Znojkiewicz',
      street: 'os.Cegielskiego 39/5',
      city: 'Poznań',
      phone: '722870783',
      process: true,
    },
    {
      id: 2,
      name: 'Patryk Marcinowski',
      street: 'os.Marcinkowskiego 5',
      city: 'Czerwonak',
      phone: '827172277',
      process: false,
    },
  ],
}
const serwisSlice = createSlice({
  name: 'klienci',
  initialState,
  reducers: {
    userAdded: (state, action) => {
      state.push(action.payload)
    }
  },
})

export const { userAdded  } = serwisSlice.actions



export default serwisSlice.reducer
