import * as firebase from 'firebase/firebase'
// import firebase from "firebase/app";
import "firebase/auth";
const firebaseConfig = {
        apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
        authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAIN,
        projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
        storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
        messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
        appId: process.env.REACT_APP_FIREBASE_APP_ID,
        measurementId: process.env.REACT_APP_FIREBSE_MEASUREMENT_ID
};
firebase.initializeApp(firebaseConfig);
const facebook = new firebase.auth.FacebookAuthProvider();
const google = new firebase.auth.GoogleAuthProvider();



export const authLogin = () => {
    return (
        firebase
          .auth()
          .signInWithPopup(facebook)
          .catch((error) => {
              let errorCode = error.code;
              let errorMessage = error.message;
              console.log(`Error code: ${errorCode} Error message: ${errorMessage}`);

  })
  );


}

export const auth = firebase.auth