export const theme = {
  fontSize: {
    title: '30px',
    xxl: '25px',
    xl: '21px',
    l: '18px',
    m: '16px',
    s: '14px',
  },
  fontWeight: {
    ligth: 300,
    medium: 500,
    bold: 700,
  },
  colors: {
    red: '#CF350E',
    yellow: '#ffd166',
    green: '#06d6a0',
    ligthBlue: '#5CEBF0',
    blue: '#3894C9',
    darkBlue: '#223E62',
    darkGrey: '#718598',
    ligthGrey: '#e5e5e5',
    grey: '#718598',
    black: '#172234',
    ligthWhite: '#f5f5f5',
    white: '#fff',
    facebook: '#3b5998',
  },
  shadow: {
    light: '2px 4px 5px rgba(115, 124, 142, 0.29)',
    dark: '1px 1px 10px rgb(116 124 142 / 50%)',
  },
}
