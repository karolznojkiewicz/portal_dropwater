import { Button } from 'components/atoms/Button/Button.styles'
import HeaderTable from 'components/molecules/HeaderTable/HeaderTable'
import Table from 'components/organism/Table/Table'
import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'

const Serwis = () => {
  const dataClient = useSelector((person) => person.dropWater.serwisy)
  const columns = useMemo(
    () => [
      {
        Header: 'Id',
        accessor: 'id',
      },
      {
        Header: 'Imię i nazwisko',
        accessor: 'name',
      },
      {
        Header: 'Ulica',
        accessor: 'street',
      },
      {
        Header: 'Miasto',
        accessor: 'city',
      },
      {
        Header: 'Telefon',
        accessor: 'phone',
      },
      {
        Header: 'Wykonanie',
        accessor: 'process',
        Cell: ({ cell: { value } }) => {
          return (
            <>
              {value ? (
                <Button primary>Wykonane</Button>
              ) : (
                <Button error>Niewykonane</Button>
              )}
            </>
          )
        },
      },
    ],
    []
  )

  return (
    <>
      <HeaderTable title="Klienci" dataClient={dataClient} />
      <Table columns={columns} data={dataClient} />
    </>
  )
}

export default Serwis
