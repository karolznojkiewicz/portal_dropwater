import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import FormField from 'components/molecules/FormField/FormField'
import { nanoid } from '@reduxjs/toolkit'
import { userAdded } from 'features/serwis/serwisSlice'
import { Select } from 'components/atoms/Select/Select.styles'
import { Button } from 'components/atoms/Button/Button.styles'

const Wrapper = styled.div`
  max-width: 1000px;
  width: 100%;
`
const Form = styled.form`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 50px;
  grid-row-gap: 25px;
`

const AddedPerson = () => {
  const [usersValue, setUsersValue] = useState({
    name: '',
    surname: '',
    street: '',
    city: '',
    phone: '',
  })
  const dispatch = useDispatch()

  const handleSubmit = (e) => {
    e.preventDefault()

    const newUser = {
      name: usersValue.name,
      surname: usersValue.surname,
      street: usersValue.street,
      city: usersValue.city,
      phone: usersValue.phone,
    }
    console.log(newUser)
    if (usersValue) {
      dispatch(
        userAdded({
          id: nanoid(),
          ...newUser,
        })
      )
    }
  }

  const handleInputChange = (e) => {
    setUsersValue({
      ...usersValue,
      [e.target.name]: e.target.value,
    })
  }
  return (
    <Wrapper>
      <Form onSubmit={handleSubmit}>
        <FormField
          label="Imię"
          name="name"
          value={usersValue.name}
          onChange={handleInputChange}
        />
        <FormField
          label="Nazwisko"
          name="surname"
          value={usersValue.surname}
          onChange={handleInputChange}
        />

        <Select id="cars">
          <option value="volvo">ul.</option>
          <option value="saab">os.</option>
        </Select>

        <FormField
          label="Adres"
          name="street"
          value={usersValue.street}
          onChange={handleInputChange}
        />

        <FormField
          label="Miasto"
          name="city"
          value={usersValue.city}
          onChange={handleInputChange}
        />
        <FormField
          label="Telefon"
          name="phone"
          type="number"
          value={usersValue.phone}
          onChange={handleInputChange}
        />
        <FormField
          label="Telefon"
          name="phone2"
          type="number"
          value={usersValue.phone}
          onChange={handleInputChange}
        />
        <FormField
          label="Data zakupu"
          name="date_buy"
          type="date"
          value={usersValue.phone}
          onChange={handleInputChange}
        />
        <FormField
          label="Serwisowanie"
          name="date_serwis"
          type="number"
          value={usersValue.phone}
          onChange={handleInputChange}
        />
        <Button primary type="submit">
          Dodaj klienta
        </Button>
      </Form>
    </Wrapper>
  )
}

export default AddedPerson
