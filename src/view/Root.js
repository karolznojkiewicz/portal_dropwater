import React from 'react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle } from 'assets/styles/globalStyle'
import { theme } from 'assets/styles/theme'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import MainTemplates from 'components/templates/MainTemplates/MainTemplates'
import Dashbord from './Dashbord'
import People from './People'
import Serwis from './Serwis'
import AddedPerson from './AddedPerson'
import LoginForm from 'components/templates/LoginForm/LoginForm'
import { Wrapper } from './Root.styles'

import useAuthUser from 'Hooks/useAuthUser'

function App() {
  const currentUser = useAuthUser()
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
              {currentUser ? (
                <>
                <MainTemplates>
                 <Wrapper>
                  <Switch>
                    <Route exact path="/">
                      <Dashbord />
                    </Route>
                    <Route path="/people">
                      <People />
                    </Route>
                    <Route path="/serwis">
                      <Serwis />
                    </Route>
                    <Route path="/add">
                      <AddedPerson />
                    </Route>
                     </Switch>
                    </Wrapper>
                     </MainTemplates>
                    </>
              ): (
                <>

                <Route path="/login" component={LoginForm} />
                <Redirect to="/login" />
                </>
              )}





      </ThemeProvider>
    </Router>
  )
}

export default App
