import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import HeaderTable from 'components/molecules/HeaderTable/HeaderTable'
import Table from 'components/organism/Table/Table'
import { PhoneStyle } from 'components/atoms/BodyTable/Tbody.styles'

const People = () => {
  const columns = useMemo(
    () => [
      {
        Header: 'Id',
        accessor: 'id',
      },
      {
        Header: 'Imię i nazwisko',
        accessor: 'name',
      },
      {
        Header: 'Ulica',
        accessor: 'street',
      },
      {
        Header: 'Miasto',
        accessor: 'city',
      },
      {
        Header: 'Telefon',
        accessor: 'phone',
        Cell: ({ cell: { value } }) => {
          return (
            <>
              {value.map((item, id) => (
                <PhoneStyle key={id}>{item}</PhoneStyle>
              ))}
            </>
          )
        },
      },
    ],
    []
  )
  const listClient = useSelector((state) => state.dropWater.klienci)
  return (
    <>
      <HeaderTable title="Klienci" dataClient={listClient} />
      <Table columns={columns} data={listClient} />
    </>
  )
}

export default People
