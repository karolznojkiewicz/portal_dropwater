import styled from 'styled-components'

export const Wrapper = styled.section`
  grid-area: Main;
  padding: 50px 70px;
  overflow-x: hidden;
`
