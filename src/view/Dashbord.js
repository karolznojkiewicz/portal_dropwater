import React from 'react'
import styled from 'styled-components'

const WrapperBox = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 50px;
`
const Box = styled.div`
  background-color: ${({ theme }) => theme.colors.white};
  width: 400px;
  height: 300px;
  text-align: center;
  h3 {
    font-weight: ${({ theme }) => theme.fontWeight.bold};
  }
  p {
    font-size: ${({ theme }) => theme.fontSize.xxl};
    background-color: ${({ theme }) => theme.colors.blue};
    margin: 20px 30px;
    padding: 20px 0;
    color: ${({ theme }) => theme.colors.white};
    border-radius: 20px;
  }
`

const Dashbord = () => {
  return (
    <WrapperBox>
      <Box>
        <h3>Zarobek w miesiącu</h3>
        <p>10000zł </p>
      </Box>
      <Box>
        <h3>Sprzedaż w roku</h3>
        <p>123 szt </p>
      </Box>
      <Box>
        <h3>Zarobek w miesiącu</h3>
        <p>10000zł </p>
      </Box>
    </WrapperBox>
  )
}

export default Dashbord
