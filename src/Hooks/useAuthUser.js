import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { loginIn, logout, selectCurrentUser } from 'features/serwis/authSlice'
import {auth} from 'services/config'
import { useHistory } from 'react-router'

export default () => {
    const dispatch = useDispatch()
    const currentUser = useSelector(selectCurrentUser)


    useEffect(() => {
        const setUser = (user) => {
            if(user){
                dispatch(loginIn({
                uid: user.uid,
                email: user.email,
                name: user.displayName
                }))
            }else{
                dispatch(logout())

            }
        }
        const unsubscribe = auth().onAuthStateChanged(setUser);
        return () => unsubscribe()


  }, [dispatch])

  return currentUser
}