import React from 'react'
import Table from './Table'

export default {
  title: 'Components/Templates/Table',
  component: 'Table',
}

const Template = (args) => <Table {...args} />

export const Default = Template.bind({})
Default.args = {}
