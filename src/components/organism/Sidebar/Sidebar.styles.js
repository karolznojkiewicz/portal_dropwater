import styled from 'styled-components'

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  border-right: 1px solid #eee;
  padding: 20px 0;
  background-color: ${({ theme }) => theme.colors.black};
  width: 250px;
`
