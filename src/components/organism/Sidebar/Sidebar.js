import React from 'react'
import Logo from 'components/atoms/Logo/Logo'
import { Wrapper } from './Sidebar.styles'
import Navigation from 'components/molecules/Navigation/Navigation'

const Sidebar = () => {
  return (
    <Wrapper>
      <Logo />
      <Navigation />
    </Wrapper>
  )
}

export default Sidebar
