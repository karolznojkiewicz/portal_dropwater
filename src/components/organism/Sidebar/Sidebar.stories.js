import React from 'react'
import Sidebar from './Sidebar'

export default {
  title: 'Components/Organism/Sidebar',
  component: 'Sidebar',
}

const Template = (args) => <Sidebar {...args} />

export const LeftSidebar = Template.bind({})

LeftSidebar.args = {
  primary: true,
}
