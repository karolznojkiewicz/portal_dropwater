import React from 'react'
import { Button } from 'components/atoms/Button/Button.styles'
import { FiFacebook, FiLogIn } from 'react-icons/fi'
import { SiGoogle } from 'react-icons/si'
import FormField from 'components/molecules/FormField/FormField'
import { Content, Form, WrapForm, Wrapper } from './LoginForm.styles'

import {authLogin} from 'services/config'

const LoginForm = () => {
  return (
    <Wrapper>
      <Content>
          <h1>Logowanie</h1>
          <Button facebook onClick={authLogin}>
            <FiFacebook/>
            Facebook
          </Button>
          <Button google onClick={authLogin}>
            <SiGoogle />
            Google
          </Button>
          <WrapForm>
          <Form>
            <FormField label="Nazwa użytkownika/email" />
            <FormField label="Hasło" />
            <Button login type="submit">
              <FiLogIn />
              Zaloguj się
            </Button>
            </Form>
          </WrapForm>
      </Content>
    </Wrapper>
  )
}

export default LoginForm
