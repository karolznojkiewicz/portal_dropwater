import styled from 'styled-components'
import background from 'assets/image/waterbackground.jpg'

export const Wrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background: url(${background});
  padding: 15px;
`

export const Content = styled.div`
  width: 680px;
  background-color: ${({ theme }) => theme.colors.white};
  background-position: center;
  background-size: cover;
  border-radius: 10px;
  padding: 130px;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  h1 {
    text-align: center;
    text-transform: uppercase;
    font-size: 2em;
    color: ${({ theme }) => theme.colors.black};
    padding-bottom: 50px;
    width: 100%;
  }
  svg {
    font-size: ${({ theme }) => theme.fontSize.xl};
    margin-right: 10px;
  }
`
export const Form = styled.form`


`
export const WrapForm = styled.div`
  width: 100%;
  margin-top: 40px;
`

