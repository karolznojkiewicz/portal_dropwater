import SearchBar from 'components/molecules/SearchBar/SearchBar'
import Sidebar from 'components/organism/Sidebar/Sidebar'
import React from 'react'
import { Content } from './MainTemplates.styles'


const MainTemplates = ({ children }) => {
  return (
    <>
        <Content>
          <Sidebar />
          <SearchBar />
          {children}
        </Content>
    </>
  )
}

export default MainTemplates
