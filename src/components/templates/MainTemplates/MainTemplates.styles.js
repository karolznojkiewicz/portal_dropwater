import styled from "styled-components";

export const Content = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: 250px 1fr;
  grid-template-rows: 90px 1fr;
  grid-template-areas:
    'Logo Search'
    'Navigation Main';
`