import React from 'react'
import LinkNav from 'components/atoms/LinkNav/LinkNav'
import { IoHomeOutline } from 'react-icons/io5'
import {
  FiUserPlus,
  FiUsers,
  FiSettings,
  FiPhone,
  FiLogOut,
} from 'react-icons/fi'
import { useDispatch } from 'react-redux'
import { Nav } from './Navigation.styles'

import {logout} from 'features/serwis/authSlice'

const Navigation = () => {
  const dispatch = useDispatch()
  return (
    <Nav>
      <LinkNav title="Pulpit" exact path="/">
        <IoHomeOutline />
      </LinkNav>
      <LinkNav title="Ludzie" path="/people">
        <FiUsers />
      </LinkNav>
      <LinkNav title="Serwis" path="/serwis">
        <FiPhone />
      </LinkNav>
      <LinkNav title="Nowa osoba" path="/add">
        <FiUserPlus />
      </LinkNav>
      <LinkNav title="Ustawienia" path="/settings">
        <FiSettings />
      </LinkNav>
      <LinkNav title="Wyloguj" path="/login" onClick={() => dispatch(logout())}>
        <FiLogOut />
      </LinkNav>
    </Nav>
  )
}

export default Navigation
