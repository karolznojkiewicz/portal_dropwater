import styled from 'styled-components'

export const Nav = styled.div`
  grid-area: Navigation;
  display: flex;
  flex-direction: column;
  margin-top: 50px;
`
