import React, { useEffect, useState } from 'react'
import LoginStatus from 'components/atoms/LoginStatus/LoginStatus'
import { Wrapper, ContentDate } from './SearchBar.styles'
import { Input } from 'components/atoms/Input/Input.styles'

const SearchBar = () => {
  const [date] = useState(new Date().toLocaleDateString())
  const [time, setTime] = useState(new Date().toLocaleTimeString())
  const [finder, setFinder] = useState('')

  const handleFinder = (e) => {
    const value = e.target.value
  }

  const currentCallback = () => {
    const date = new Date()
    setTime(date.toLocaleTimeString())
  }

  useEffect(() => {
    const handle = setInterval(currentCallback, 1000)
    return () => clearInterval(handle)
  }, [])

  return (
    <Wrapper>
      <LoginStatus />
      <Input
        placeholder="Szukanie klienta"
        value={finder}
        onChange={handleFinder}
      />
      <ContentDate>
        <span>{date}</span>
        <span>{time}</span>
      </ContentDate>
    </Wrapper>
  )
}

export default SearchBar
