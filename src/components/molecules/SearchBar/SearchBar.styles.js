import styled from 'styled-components'

export const Wrapper = styled.div`
  grid-area: Search;
  display: flex;
  padding: 0 30px;
  align-items: center;
  box-shadow: ${({ theme }) => theme.shadow.light};
`

export const ContentDate = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  text-align: right;
`
