import React from 'react'
import { Button } from 'components/atoms/Button/Button.styles'
import { IoIosArrowForward, IoIosArrowBack } from 'react-icons/io'
import { Wrapper } from './Pagination.styles'
const Pagination = ({
  canPreviousPage,
  canNextPage,
  pageOptions,
  nextPage,
  previousPage,
  setPageSize,
  pageIndex,
  pageSize,
}) => {
  return (
    <Wrapper>
      <div>
        <span>
          Strona
          <strong>
            {pageIndex} z {pageOptions.length}
          </strong>
        </span>
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value))
          }}
        >
          {[10, 20, 30, 40, 50].map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              {pageSize}
            </option>
          ))}
        </select>
      </div>
      <div>
        <Button
          pagination
          onClick={() => previousPage()}
          disabled={!canPreviousPage}
        >
          <IoIosArrowBack />
        </Button>
        <Button pagination onClick={() => nextPage()} disabled={!canNextPage}>
          <IoIosArrowForward />
        </Button>
      </div>
    </Wrapper>
  )
}

export default Pagination
