import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'components/atoms/Input/Input.styles'
import { BoxForm } from './FormField.styles'
import { Label } from 'components/atoms/Label/Label.styles'

const FormField = ({ id, label, name, type = 'text', value, onChange }) => {
  return (
    <BoxForm>
      <Label htmlFor={id}>{label}</Label>
      <Input
        name={name}
        id={id}
        type={type}
        value={value}
        onChange={onChange}
      />
    </BoxForm>
  )
}

FormField.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
}

export default FormField
