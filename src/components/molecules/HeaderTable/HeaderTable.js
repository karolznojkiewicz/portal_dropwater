import React from 'react'
import QuantityPeople from 'components/atoms/QuantityPeople/QuantityPeople'
import { Title } from 'components/atoms/Title/Title.styles'
import { Wrapper } from './HeaderTable.styles'

const HeaderTable = ({ title }) => {
  return (
    <Wrapper>
      <Title>{title}</Title>
      <QuantityPeople />
    </Wrapper>
  )
}

export default HeaderTable
