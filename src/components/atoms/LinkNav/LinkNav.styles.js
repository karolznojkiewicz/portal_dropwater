import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

export const LinkNavigation = styled(NavLink).attrs({ active: 'active' })`
  display: flex;
  align-items: center;
  text-decoration: none;
  color: ${({ theme }) => theme.colors.white};
  font-size: ${({ theme }) => theme.fontSize.m};
  font-weight: ${({ theme }) => theme.fontWeight.medium};
  transition: all 0.1s ease-out;
  border-right: 4px solid transparent;
  padding: 15px 30px;
  &:not(:last-child) {
    border-bottom: 2px solid ${({ theme }) => theme.colors.darkBlue};
  }

  &:hover {
    background-color: ${({ theme }) => theme.colors.blue};
    color: ${({ theme }) => theme.colors.white};
    text-decoration: none;
  }
  &.active {
    background-color: ${({ theme }) => theme.colors.blue};
    color: ${({ theme }) => theme.colors.white};
    text-decoration: none;
    /* box-shadow: ${({ theme }) => theme.shadow.ligth}; */
  }

  svg {
    margin-right: 10px;
  }
`
