import React from 'react'
import LinkNav from './LinkNav'

export default {
  title: 'Components/Atoms/LinkNav',
  component: 'LinkNav',
}

const Template = (args) => <LinkNav {...args} />

export const Default = Template.bind({})
Default.args = {}
