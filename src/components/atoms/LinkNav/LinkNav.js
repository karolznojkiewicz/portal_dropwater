import React from 'react'
import { LinkNavigation } from './LinkNav.styles'

const LinkNav = ({ children, title, path, exact, onClick }) => {
  return (
    <LinkNavigation exact={exact} to={path} onClick={onClick}>
      {children}
      {title}
    </LinkNavigation>
  )
}

export default LinkNav
