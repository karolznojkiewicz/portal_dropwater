import styled from 'styled-components'

export const Input = styled.input`
  border: 1px solid ${({ theme }) => theme.colors.ligthGrey};
  padding: 15px;
  border-radius: 10px;
`
