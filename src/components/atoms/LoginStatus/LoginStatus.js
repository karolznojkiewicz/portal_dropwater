import React from 'react';
import { useSelector } from 'react-redux';
import { Box } from './LoginStatus.styles';

const LoginStatus = () => {
    const nameuser = useSelector(state => state.auth.user.name)
    return (
        <Box>
            <p>Zalogowany/a jako:</p>
            <p>{nameuser}</p>
        </Box>
    );
};

export default LoginStatus;