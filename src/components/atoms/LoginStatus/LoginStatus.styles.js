import styled from 'styled-components';

export const Box = styled.div`
    display: flex;
    flex-direction: column;
    p {
        margin: 0;
        font-size: ${({ theme }) => theme.fontSize.l};
        padding: 3px 50px;
    }
    p:first-child {
        font-size: ${({ theme }) => theme.fontSize.m};
    }
    p:nth-child(2) {
        font-weight: ${({ theme }) => theme.fontWeight.bold};
    }
`;
