import styled from 'styled-components'
// import { MdKeyboardArrowDown, MdKeyboardArrowUp } from 'react-icons/md'

export const Thead = styled.thead`
  border-bottom: 1px solid ${({ theme }) => theme.colors.ligthGrey};
  border-top: 1px solid ${({ theme }) => theme.colors.ligthGrey};
  tr {
    vertical-align: center;
  }
  tr th {
    text-align: left;
    padding: 20px 0;
    font-size: ${({ theme }) => theme.fontSize.l};
    color: ${({ theme }) => theme.colors.Grey};

    span {
      float: right;
      margin-right: 40px;
    }
  }
`
