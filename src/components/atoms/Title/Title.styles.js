import styled from 'styled-components'

export const Title = styled.h2`
  font-size: ${({ theme }) => theme.fontSize.title};
  font-weight: ${({ theme }) => theme.fontWeight.bold};
`
