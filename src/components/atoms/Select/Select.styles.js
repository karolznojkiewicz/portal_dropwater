import styled from 'styled-components'

export const Select = styled.select`
  border: ${({ theme }) => theme.colors.grey};
`
