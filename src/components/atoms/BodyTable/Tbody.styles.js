import styled from 'styled-components'

export const Tbody = styled.tbody`
  tr {
    background-color: ${({ theme }) => theme.colors.white};
    border-bottom: 2px solid ${({ theme }) => theme.colors.ligthGrey};
    transition: all 0.2s ease-in;
    &:hover {
      background-color: ${({ theme }) => theme.colors.ligthWhite};
    }
  }
  tr td {
    text-align: left;
    padding: 20px 0;
    font-weight: ${({ theme }) => theme.fontWeight.ligth};
    :nth-child(2) {
      font-weight: ${({ theme }) => theme.fontWeight.medium};
    }
  }
`
export const PhoneStyle = styled.span`
  display: block;
  font-size: ${({ theme }) => theme.fontSize.xl};
  border-radius: 10px;
  padding: 5px 0;
  max-width: max-content;
  margin: 5px 0;
`
