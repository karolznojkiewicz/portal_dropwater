import styled from 'styled-components'

export const Label = styled.label`
  font-size: ${({ theme }) => theme.fontSize.s};
  font-weight: ${({ theme }) => theme.fontWeight.bold};
  color: ${({ theme }) => theme.colors.black};
  padding-bottom: 5px;
  padding-top: 15px;
  text-transform: uppercase;
`
