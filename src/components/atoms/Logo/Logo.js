import React from 'react';
import { LogoStyles } from './Logo.styles';

const Logo = () => {
    return <LogoStyles>DropWater</LogoStyles>;
};

export default Logo;
