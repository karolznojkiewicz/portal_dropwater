import styled from 'styled-components'

export const LogoStyles = styled.h1`
  grid-area: Logo;
  font-size: ${({ theme }) => theme.fontSize.xxl};
  font-weight: ${({ theme }) => theme.fontWeight.bold};
  color: ${({ theme }) => theme.colors.white};
  padding: 10px 0;
  text-align: center;
`
