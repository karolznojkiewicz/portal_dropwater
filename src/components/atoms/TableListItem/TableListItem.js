import React from 'react'
import { IoTrashOutline } from 'react-icons/io5'
import { FiEdit } from 'react-icons/fi'

const TableListItem = ({ id, name, surname, street, city, phone }) => {
  return (
    <tr>
      <td>{id}</td>
      <td>
        <span>
          {name} {surname}
        </span>
      </td>
      <th>{street}</th>
      <th>{city}</th>
      <th>{phone}</th>
      <td>
        <IoTrashOutline />
        <FiEdit />
      </td>
    </tr>
  )
}

export default TableListItem
