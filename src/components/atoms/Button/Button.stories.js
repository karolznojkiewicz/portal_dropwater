import React from 'react'
import { Button } from './Button.styles'

export default {
  title: 'Components/Atoms/Button',
  component: 'Button',
}

const Template = (args) => <Button {...args}>Read more</Button>

export const Default = Template.bind({})

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
}

export const Error = Template.bind({})
Error.args = {
  error: true,
}

export const Facebook = Template.bind({})
Facebook.args = {
  facebook: true,
}

export const Google = Template.bind({})
Google.args = {
  google: true,
}
