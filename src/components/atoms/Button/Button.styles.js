import styled, { css } from 'styled-components'

export const Button = styled.button`
  border: none;
  border-radius: 10px;
  width: 100%;
  padding: 20px 0;
  font-weight: ${({ theme }) => theme.fontWeight.bold};
  transition: all 0.2s ease-in;
  ${({ pagination }) =>
    pagination &&
    css`
      background-color: transparent;

      color: ${({ theme }) => theme.colors.black};
      :hover {
        background-color: transparent;

        color: ${({ theme }) => theme.colors.black};
      }
    `}
  ${({ primary }) =>
    primary &&
    css`
      background-color: ${({ theme }) => theme.colors.blue};
      color: ${({ theme }) => theme.colors.white};

      padding: 5px 10px;
    `}
    ${({ error }) =>
    error &&
    css`
      background-color: ${({ theme }) => theme.colors.red};
      color: ${({ theme }) => theme.colors.white};
      padding: 5px 10px;
    `}
    ${({ facebook }) =>
    facebook &&
    css`
      color: ${({ theme }) => theme.colors.white};
      background-color: ${({ theme }) => theme.colors.facebook};
      width: calc((100% - 20px) / 2);
      font-size: ${({ theme }) => theme.fontSize.l};
      display: flex;
      align-items: center;
      justify-content: center;
    `}
    ${({ google }) =>
    google &&
    css`
      color: ${({ theme }) => theme.colors.darkGrey};
      background-color: ${({ theme }) => theme.colors.white};
      width: calc((100% - 20px) / 2);
      font-size: ${({ theme }) => theme.fontSize.l};
      display: flex;
      align-items: center;
      justify-content: center;
      box-shadow: ${({ theme }) => theme.shadow.dark};
    `}
    ${({ login }) =>
    login &&
    css`
      color: ${({ theme }) => theme.colors.white};
      background-color: ${({ theme }) => theme.colors.blue};
      font-size: ${({ theme }) => theme.fontSize.l};
      display: flex;
      align-items: center;
      justify-content: center;

      margin-top: 20px;
      :hover {
        box-shadow: ${({ theme }) => theme.shadow.dark};
      }
    `};
`
