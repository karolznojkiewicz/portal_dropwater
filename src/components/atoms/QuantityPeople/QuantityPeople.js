import React from 'react'
import { useSelector } from 'react-redux'
import { Quantity } from './QuantityPeople.styles'

const QuantityPeople = () => {
  const data = useSelector((person) => person.dropWater.klienci)
  return <Quantity>{data.length} Klientów</Quantity>
}

export default QuantityPeople
